package dev.handschrift.tickets

import dev.handschrift.tickets.data.TicketKind
import org.bson.codecs.pojo.annotations.BsonId
import org.litote.kmongo.findOneById
import org.litote.kmongo.save

class ServerConfig
    (
    @BsonId
    val guildId: String,
    var categoryId: String? = null,
    var exportChannel: String? = null,
    var defaultTicketName: String = "ticket",
    var startCount: Int = 0,
    var maxTicketsPerUser: Int? = null,
    var ticketWelcomeMessage: String = "Support will be with you shortly.",
    var moderatorRoles: MutableList<String> = mutableListOf(),
    val ticketKinds: MutableList<TicketKind> = mutableListOf()
) {
    companion object {
        fun get(guildId: String): ServerConfig {
            return dataStoreConfig.findOneById(guildId) ?: ServerConfig(guildId = guildId)
        }
    }

    fun save() {
        dataStoreConfig.save(this)
    }

}