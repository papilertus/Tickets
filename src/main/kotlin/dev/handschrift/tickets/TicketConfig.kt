package dev.handschrift.tickets

data class TicketConfig(
    val lang: String,
    val useSeparators: Boolean
)