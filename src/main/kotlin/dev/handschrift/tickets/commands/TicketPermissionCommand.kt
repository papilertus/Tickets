package dev.handschrift.tickets.commands

import com.papilertus.command.Command
import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.getLocalizedString
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.OptionData

class TicketPermissionCommand :
    Command("ticket-permission", "Sets the permitted roles of the ticket command", true, true, false, true) {

    init {
        commandData
            .addOptions(
                OptionData(OptionType.STRING, "operation", "Adding or removing", true)
                    .addChoice("add", "add")
                    .addChoice("remove", "remove"),
                OptionData(OptionType.ROLE, "role", "role", true)
            )
    }

    override fun execute(event: SlashCommandInteractionEvent): Boolean {

        val role = event.getOption("role")!!.asRole
        val config = ServerConfig.get(event.guild!!.id)

        when (event.getOption("operation")!!.asString) {

            "add" -> {
                if (!config.moderatorRoles.contains(role.id)) {
                    config.moderatorRoles.add(role.id)
                    config.save()
                    event.reply(getLocalizedString("ticketpermissioncommand.role_add", role.asMention)).setEphemeral(true).queue()
                } else {
                    event.reply(getLocalizedString("ticketpermissioncommand.role_already_in", role.asMention))
                        .setEphemeral(true).queue()
                }
            }

            "remove" -> {
                if (config.moderatorRoles.contains(role.id)) {
                    config.moderatorRoles.remove(role.id)
                    config.save()
                    event.reply(getLocalizedString("ticketpermissioncommand.role_remove", role.asMention)).setEphemeral(true).queue()
                } else {
                    event.reply(getLocalizedString("ticketpermissioncommand.role_not_in", role.asMention)).setEphemeral(true).queue()
                }
            }
        }

        return true
    }
}