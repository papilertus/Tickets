package dev.handschrift.tickets.commands

import com.papilertus.command.Command
import com.papilertus.gui.modal.DiscordModal
import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.getLocalizedString
import dev.minn.jda.ktx.interactions.components.TextInput
import dev.minn.jda.ktx.messages.Embed
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.emoji.Emoji
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.components.buttons.Button
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle

class TicketInitCommand : Command("ticket-init", "Sends the ticket message", true, true, false, false) {

    override fun execute(event: SlashCommandInteractionEvent): Boolean {

        val config = ServerConfig.get(event.guild!!.id)

        val roleIds = event.member!!.roles.map { it.id }

        if (config.moderatorRoles.none { roleIds.contains(it) } && !event.member!!.hasPermission(Permission.ADMINISTRATOR)) {
            event.reply(getLocalizedString("ticketinitcommand.no_permission")).setEphemeral(true).queue()
            return false
        }

        val titleInput = TextInput("title", getLocalizedString("ticketinitcommand.modal.title"), TextInputStyle.SHORT, false)
        val descriptionInput = TextInput(
            "description", getLocalizedString("ticketinitcommand.modal.description"), TextInputStyle.PARAGRAPH, true,
            value = getLocalizedString("ticketinitcommand.click_to_create")
        )

        val modal = DiscordModal(event.user.id, getLocalizedString("ticketinitcommand.modal_title"), {
            val title = it.getValue("title")!!.asString.ifEmpty { null }
            val description = it.getValue("description")!!.asString

            val embed = Embed {
                this.title = title
                this.description = description
            }

            it.channel.sendMessageEmbeds(embed)
                .addActionRow(Button.of(ButtonStyle.PRIMARY, "BUTTON_CREATE_TICKET", getLocalizedString("ticketinitcommand.button_text"), Emoji.fromUnicode("\uD83C\uDD95"))).queue()
            it.deferEdit().queue()
        }, titleInput, descriptionInput)

        event.replyModal(modal.buildModal()).queue()

        return true
    }
}