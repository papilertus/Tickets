package dev.handschrift.tickets.commands

import com.papilertus.command.Command
import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.data.GuildTicket
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent

abstract class AbstractTicketCommand(name: String, description: String, private val moderatorOnly: Boolean = false) :
    Command(name, description) {
    override fun execute(event: SlashCommandInteractionEvent): Boolean {
        val config = ServerConfig.get(event.guild!!.id)
        var permitted = true
        if (moderatorOnly) {
            val roleIds = event.member!!.roles.map { it.id }
            permitted =
                config.moderatorRoles.any { roleIds.contains(it) } || event.member!!.hasPermission(Permission.ADMINISTRATOR)

        }
        return permitted && GuildTicket.get(event.guild!!.id).tickets.any { it.channelId == event.channel.id }
    }
}