package dev.handschrift.tickets.commands

import com.papilertus.command.Command
import com.papilertus.gui.modal.DiscordModal
import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.data.GuildTicket
import dev.handschrift.tickets.data.TicketKind
import dev.handschrift.tickets.getLocalizedString
import dev.minn.jda.ktx.interactions.components.TextInput
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.channel.ChannelType
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.OptionData
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle

class TicketSettingsCommand : Command("ticket-settings", "settings", true, true, false, false) {

    init {
        commandData.addSubcommands(
            SubcommandData("category", "category")
                .addOption(OptionType.CHANNEL, "category", "category", true),
            SubcommandData("purge", "Deletes all tickets and all settings"),
            SubcommandData("start-count", "Sets the start of the ticket count ids")
                .addOption(OptionType.INTEGER, "count", "count", true),
            SubcommandData("welcome-message", "Sets the welcome message for a ticket"),
            SubcommandData("tickets-per-user", "Sets the maximum amounts of tickets a user can create")
                .addOption(OptionType.INTEGER, "count", "Maximum", true),
            SubcommandData("export-channel", "Sets the export channel for ticket exports")
                .addOption(OptionType.CHANNEL, "channel", "Export channel", true),
            SubcommandData("ticket-kind", "Adds a ticket kind a user can choose from")
                .addOptions(
                    OptionData(OptionType.STRING, "operation", "operation", true)
                        .addChoice("add", "add")
                        .addChoice("remove", "remove"),
                    OptionData(OptionType.STRING, "kind", "kind", true)
                ),
            SubcommandData("show", "Shows the current ticket settings")

        )
    }

    override fun execute(event: SlashCommandInteractionEvent): Boolean {
        val config = ServerConfig.get(event.guild!!.id)

        val roleIds = event.member!!.roles.map { it.id }

        if (config.moderatorRoles.none { roleIds.contains(it) } && !event.member!!.hasPermission(Permission.ADMINISTRATOR)) {
            event.reply(getLocalizedString("ticketsettingscommand.no_permission")).setEphemeral(true).queue()
            return false
        }

        when (event.subcommandName) {
            "category" -> {
                val category = event.getOption("category")!!.asChannel

                if (category.type != ChannelType.CATEGORY) {
                    event.reply(getLocalizedString("ticketsettingscommand.no_category")).setEphemeral(true).queue()
                    return false
                }

                config.categoryId = category.id
                event.reply(getLocalizedString("ticketsettingscommand.category_set", category.name)).setEphemeral(true)
                    .queue()
            }

            "start-count" -> {
                val number = event.getOption("count")!!.asInt
                config.startCount = number

                event.reply(getLocalizedString("ticketsettingscommand.startcount_set", number)).setEphemeral(true)
                    .queue()
            }

            "purge" -> {
                val guildTickets = GuildTicket.get(event.guild!!.id)

                for (ticket in guildTickets.tickets) {
                    ticket.channelId?.let { event.guild!!.getTextChannelById(it)?.delete()?.queue() }
                }
                guildTickets.delete()
                event.reply(getLocalizedString("ticketsettingscommand.purged")).setEphemeral(true).queue()
            }

            "welcome-message" -> {
                val messageInput =
                    TextInput("message", "Message", TextInputStyle.PARAGRAPH, true, value = config.ticketWelcomeMessage)
                val modal = DiscordModal(event.user.id, "Message", {
                    config.ticketWelcomeMessage = it.getValue("message")!!.asString
                    it.deferEdit()
                }, messageInput)

                event.replyModal(modal.buildModal()).queue()
            }

            "export-channel" -> {
                val channel = event.getOption("channel")!!.asChannel

                if (channel.type != ChannelType.TEXT) {
                    event.reply(getLocalizedString("ticketsettingscommand.textchannel")).setEphemeral(true).queue()
                    return false
                }
                config.exportChannel = channel.id
                event.reply(getLocalizedString("ticketsettingscommand.exportchannel_set", channel.name))
                    .setEphemeral(true).queue()
            }

            "tickets-per-user" -> {
                val count = event.getOption("count")!!.asInt
                if (count < 1) {
                    config.maxTicketsPerUser = null
                    event.reply(getLocalizedString("ticketsettingscommand.startcount_ltone")).setEphemeral(true).queue()
                    return true
                }

                config.maxTicketsPerUser = count
                event.reply(getLocalizedString("ticketsettingscommand.max_tickets_set", count)).setEphemeral(true)
                    .queue()

            }

            "ticket-kind" -> {
                val kind = event.getOption("kind")!!.asString
                val config = ServerConfig.get(event.guild!!.id)

                when (event.getOption("operation")!!.asString) {

                    "add" -> {
                        if (config.ticketKinds.find { it.name == kind } == null) {
                            val nameInput = TextInput("name", "Name", TextInputStyle.SHORT, true, value = kind)
                            val messageInput = TextInput("message", "Message", TextInputStyle.SHORT, true)
                            val modal = DiscordModal(event.user.id, "Add a new ticket category", {
                                val name = it.getValue("name")!!.asString
                                val message = it.getValue("message")!!.asString

                                config.ticketKinds.add(TicketKind(name, message))
                                config.save()
                                it.reply(getLocalizedString("ticketsettingscommand.kind_add", kind)).setEphemeral(true)
                                    .queue()

                            }, nameInput, messageInput)
                            event.replyModal(modal.buildModal()).queue()
                        } else {
                            event.reply(getLocalizedString("ticketsettingscommand.kind_already_exist"))
                                .setEphemeral(true).queue()
                        }
                    }

                    "remove" -> {
                        val ticketKind = config.ticketKinds.find { it.name == kind }
                        if (ticketKind != null) {
                            config.ticketKinds.remove(ticketKind)
                            config.save()
                            event.reply(getLocalizedString("ticketsettingscommand.kind.removed", kind))
                                .setEphemeral(true).queue()
                        } else {
                            event.reply(getLocalizedString("ticketsettingscommand.kind.not_exists")).setEphemeral(true)
                                .queue()
                        }
                    }
                }
            }

            "show" -> {

            }
        }

        config.save()
        return true
    }
}