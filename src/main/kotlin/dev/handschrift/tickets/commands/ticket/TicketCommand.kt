package dev.handschrift.tickets.commands.ticket

import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.commands.AbstractTicketCommand
import dev.handschrift.tickets.data.GuildTicket
import dev.handschrift.tickets.data.Ticket
import dev.handschrift.tickets.getLocalizedString
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData
import net.dv8tion.jda.api.utils.FileUpload

class TicketCommand : AbstractTicketCommand("ticket", "Adds a participant", true) {

    init {
        commandData.addSubcommands(
            SubcommandData("add", "Adds a participant")
                .addOption(OptionType.USER, "user", "User to add", true),
            SubcommandData("close", "Closes the current ticket"),
            SubcommandData("rename", "rename the ticket")
                .addOption(OptionType.STRING, "name", "New Name", true),
            SubcommandData("create", "Creates a new Ticket"),
            SubcommandData("export", "Exports a Ticket to html.")
        )
    }

    override fun execute(event: SlashCommandInteractionEvent): Boolean {

        val serverConfig = ServerConfig.get(event.guild!!.id)
        val ticketHolder = GuildTicket.get(event.guild!!.id)

        if (event.subcommandName == "create") {
            if (serverConfig.categoryId == null) {
                event.reply(getLocalizedString("ticketcommand.no_category")).setEphemeral(true).queue()
                return false
            }
            val category = event.guild!!.getCategoryById(serverConfig.categoryId!!)

            if (category == null) {
                event.reply(getLocalizedString("ticketcommand.null_category")).setEphemeral(true).queue()
                return false
            }


            val roleIds = event.member!!.roles.map { it.id }
            val permitted =
                serverConfig.moderatorRoles.any { roleIds.contains(it) } || event.member!!.hasPermission(Permission.ADMINISTRATOR)


            if (serverConfig.maxTicketsPerUser != null && !permitted) {
                if (ticketHolder.tickets.filter { it.creator == event.member!!.id }.size >= serverConfig.maxTicketsPerUser!!) {
                    event.reply(getLocalizedString("ticketcommand.max_ticket_error", serverConfig.maxTicketsPerUser!!))
                        .setEphemeral(true).queue()
                    return false
                }
            }


            val ticket =
                Ticket(0, serverConfig.defaultTicketName,"", event.member!!.id, mutableListOf(event.member!!.id))

            ticket.createChannel(category, serverConfig, ticketHolder)

            return true
        }

        if (!super.execute(event)) {
            event.reply(getLocalizedString("ticketcommand.no_permission"))
                .setEphemeral(true).queue()
            return false
        }
        val ticket = ticketHolder.tickets.find { it.channelId == event.channel.id }
        when (event.subcommandName) {
            "add" -> {
                val user = event.getOption("user")!!.asUser
                ticket?.addParticipant(user.id)
                event.reply(getLocalizedString("ticketcommand.add_user", user.asMention)).setEphemeral(true).queue()
            }

            "close" -> {
                event.reply(getLocalizedString("ticketcommand.closed")).setEphemeral(true).complete()
                ticket?.close()
            }

            "rename" -> {
                val name = event.getOption("name")!!.asString
                ticket?.rename(name)
                event.reply(getLocalizedString("ticketcommand.channel_renamed", name)).setEphemeral(true).queue()
            }

            "export" -> {
                val export = ticket!!.toHtml()

                event.reply(getLocalizedString("ticketcommand.history_export"))
                    .addFiles(FileUpload.fromData(export!!.toByteArray(), "${ticket.ticketId}-${ticket.name}.html"))
                    .setEphemeral(true).queue()
                return true
            }
        }
        ticketHolder.save()
        return true
    }
}