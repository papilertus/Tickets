package dev.handschrift.tickets

import com.mongodb.client.MongoCollection
import com.papilertus.command.Command
import com.papilertus.gui.contextMenu.ContextMenuEntry
import com.papilertus.plugin.Plugin
import com.papilertus.plugin.PluginData
import dev.handschrift.tickets.commands.TicketInitCommand
import dev.handschrift.tickets.commands.TicketPermissionCommand
import dev.handschrift.tickets.commands.TicketSettingsCommand
import dev.handschrift.tickets.commands.ticket.TicketCommand
import dev.handschrift.tickets.data.GuildTicket
import dev.handschrift.tickets.listeners.ButtonListener
import dev.handschrift.tickets.listeners.ChannelListener
import dev.handschrift.tickets.listeners.RoleRemoveListener
import net.dv8tion.jda.api.hooks.EventListener
import java.text.MessageFormat
import java.util.*

lateinit var dataStoreConfig: MongoCollection<ServerConfig>
lateinit var ticketStore: MongoCollection<GuildTicket>
lateinit var ticketConfig: TicketConfig

class TicketsPlugin : Plugin {
    override fun getCommands(): List<Command> {
        return listOf(
            TicketSettingsCommand(),
            TicketInitCommand(),
            TicketCommand(),
            TicketPermissionCommand()
        )
    }

    override fun getContextMenuEntries(): List<ContextMenuEntry> {
        return listOf()
    }

    override fun getListeners(): List<EventListener> {
        return listOf(ButtonListener(), RoleRemoveListener(), ChannelListener())
    }

    override fun onLoad(data: PluginData) {
        dataStoreConfig = data.getPluginDataStore("config", ServerConfig::class.java)
        ticketStore = data.getPluginDataStore("ticket", GuildTicket::class.java)
        ticketConfig = data.registerConfig()
        println("Hello from Tickets!")
    }

    override fun onUnload() {
        println("Ticket plugin is unloading..")
    }
}
fun getLocalizedString(key: String, vararg args: Any): String{
    val res = ResourceBundle.getBundle("tickets", Locale.forLanguageTag(ticketConfig.lang), TicketsPlugin::class.java.classLoader)
    return MessageFormat.format(res.getString(key), *args)
}