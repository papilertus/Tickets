package dev.handschrift.tickets.data

import dev.handschrift.tickets.ticketStore
import org.bson.codecs.pojo.annotations.BsonId
import org.litote.kmongo.*

class GuildTicket(
    @BsonId
    val guildId: String,
    val tickets: MutableList<Ticket> = mutableListOf()
) {
    companion object {
        fun get(guildId: String): GuildTicket {
            val store = ticketStore.findOneById(guildId) ?: GuildTicket(guildId = guildId)
            for(ticket in store.tickets){
                ticket.guildTicketStore = store
            }
            return store
        }
    }

    fun addTicket(ticket: Ticket){
        ticket.guildTicketStore = this
        this.tickets.add(ticket)
    }

    fun save() {
        ticketStore.save(this)
    }

    fun delete() {
        ticketStore.deleteOneById(this.guildId)
    }
}