package dev.handschrift.tickets.data

import com.fasterxml.jackson.annotation.JsonIgnore
import com.papilertus.init.jda
import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.getLocalizedString
import dev.handschrift.tickets.ticketConfig
import dev.minn.jda.ktx.messages.Embed
import dev.minn.jda.ktx.messages.MessageCreate
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.entities.channel.concrete.Category
import net.dv8tion.jda.api.entities.emoji.Emoji
import net.dv8tion.jda.api.interactions.components.buttons.Button
import net.dv8tion.jda.api.interactions.components.buttons.ButtonStyle
import net.dv8tion.jda.api.utils.FileUpload

data class Ticket(
    var ticketId: Int,
    var name: String,
    var channelId: String? = null,
    var creator: String? = null,
    val participants: MutableList<String>,
    val kind: TicketKind? = null,
    @JsonIgnore
    var guildTicketStore: GuildTicket? = null
) {
    fun createChannel(category: Category, config: ServerConfig, holder: GuildTicket, kind: TicketKind? = null) {

        this.ticketId = Math.max(
            if (holder.tickets.isEmpty()) 0
            else holder.tickets.maxByOrNull { it.ticketId }!!.ticketId + 1, config.startCount
        )

        val channel = category.createTextChannel("${ticketId}-${name}").complete()

        if (ticketConfig.useSeparators) {
            val firstSeparator = category.textChannels.firstOrNull()
            val lastSeparator = category.textChannels.lastOrNull()

            if (firstSeparator != null && lastSeparator != null && firstSeparator != lastSeparator) {
                category.modifyTextChannelPositions().selectPosition(channel).swapPosition(lastSeparator!!).queue()
            }
        }

        val closeButton =
            Button.of(ButtonStyle.DANGER, "BUTTON_CLOSE_TICKET", getLocalizedString("ticket.button.close"), Emoji.fromUnicode("\uD83D\uDD12"))

        channelId = channel.id

        for (override in channel.permissionOverrides) {
            override.delete().complete()
        }

        for (role in config.moderatorRoles) {
            channel.upsertPermissionOverride(category.guild.getRoleById(role)!!).grant(412317371456).deny(0)
                .queue()
        }

        for (participant in participants) {
            channel.upsertPermissionOverride(category.guild.getMemberById(participant)!!).grant(412317371456)
                .deny(0).queue()
        }

        channel.upsertPermissionOverride(category.guild.publicRole).grant(0).deny(534723951681).queue()
        channel.upsertPermissionOverride(category.guild.getMemberById(category.jda.selfUser.id)!!).grant(536271126225).deny(0).queue()

        val embed = Embed {
            description = config.ticketWelcomeMessage
        }

        val message = MessageCreate {
            this.embeds.plusAssign(embed)
            this.content = config.moderatorRoles.joinToString(",") { category.guild.getRoleById(it)!!.asMention } + "\n${category.guild.getMemberById(creator!!)!!.asMention}"
        }

        channel.sendMessage(message).addActionRow(closeButton).queue()

        if (kind != null) {
            channel.sendMessage(kind.message).queue()
        }
    }

    fun addParticipant(userId: String) {
        if (channelId != null) {
            participants.add(userId)
            val channel = jda.getTextChannelById(channelId!!)
            channel!!.manager.putMemberPermissionOverride(userId.toLong(), 412317371456, 0).queue()
        }
    }

    fun rename(name: String) {
        if (channelId != null) {
            this.name = name
            val channel = jda.getTextChannelById(channelId!!)
            channel!!.manager.setName("${ticketId}-${name}").queue()
        }
    }

    @JsonIgnore
    fun getExportEmbed(): EmbedBuilder {
        return EmbedBuilder()
    }
    @JsonIgnore
    fun toHtml(): String? {
        val channel = jda.getTextChannelById(this.channelId!!) ?: return null
        val history = channel.history
        val baseHtml = this.javaClass.classLoader.getResourceAsStream("BaseExport.html")
        val baseHtmlString = String(baseHtml!!.readAllBytes())

        val builder = StringBuilder("<ul>")
        do {
            val messages = history.retrievePast(100).complete().reversed()
            for (message in messages) {
                if (!message.author.isBot && !message.author.isSystem) {
                    builder.append(
                        """
                            <li>
                                <img src="${message.author.effectiveAvatarUrl}" alt="profile picture">
                                <div class="container">
                                      <p class="name">${message.author.asTag}</p>
                                      <p class="message">${message.contentDisplay}</p>
                                </div>
                            </li>                                
                            """.trimIndent()
                    )
                }
            }
        } while (messages.size == 100)

        return baseHtmlString.replace("<ul>", builder.toString())
    }

    fun close() {
        val guildTickets = GuildTicket.get(this.guildTicketStore!!.guildId)
        val config = ServerConfig.get(this.guildTicketStore!!.guildId)
        val exportChannel =
            if (config.exportChannel == null) null else jda.getTextChannelById(config.exportChannel!!)
        val html = toHtml()
        guildTickets.tickets.removeIf { it.ticketId == this.ticketId }
        if (html != null) {
            exportChannel?.sendMessage(getLocalizedString("ticket.exported"))
                ?.addFiles(FileUpload.fromData(html.toByteArray(), "${this.ticketId}-${this.name}.html"))
                ?.queue()
        }
        guildTickets.save()
        if (channelId != null) {
            val channel = jda.getGuildChannelById(channelId!!)
            channel?.delete()?.queue()
        }
    }
}