package dev.handschrift.tickets.data

data class TicketKind(val name: String, val message: String)