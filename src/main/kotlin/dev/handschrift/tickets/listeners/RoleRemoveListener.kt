package dev.handschrift.tickets.listeners

import dev.handschrift.tickets.ServerConfig
import net.dv8tion.jda.api.events.role.RoleDeleteEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class RoleRemoveListener : ListenerAdapter() {
    override fun onRoleDelete(event: RoleDeleteEvent) {
        val config = ServerConfig.get(event.guild.id)
        if (config.moderatorRoles.contains(event.role.id)) {
            config.moderatorRoles.remove(event.role.id)
            config.save()
        }
    }
}