package dev.handschrift.tickets.listeners

import dev.handschrift.tickets.data.GuildTicket
import net.dv8tion.jda.api.events.channel.ChannelDeleteEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class ChannelListener: ListenerAdapter() {
    override fun onChannelDelete(event: ChannelDeleteEvent) {
        val tickets = GuildTicket.get(event.guild.id).tickets
        tickets.find { it.channelId == event.channel.id}?.close()
    }
}