package dev.handschrift.tickets.listeners

import dev.handschrift.tickets.ServerConfig
import dev.handschrift.tickets.data.GuildTicket
import dev.handschrift.tickets.data.Ticket
import dev.handschrift.tickets.getLocalizedString
import dev.minn.jda.ktx.interactions.components.SelectOption
import dev.minn.jda.ktx.interactions.components.StringSelectMenu
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class ButtonListener : ListenerAdapter() {
    override fun onButtonInteraction(event: ButtonInteractionEvent) {
        when (event.button.id) {
            "BUTTON_CREATE_TICKET" -> {
                val config = ServerConfig.get(event.guild!!.id)
                val ticketHolder = GuildTicket.get(event.guild!!.id)

                val id = config.categoryId

                if (id == null) {
                    event.reply(getLocalizedString("buttonlistener.configuration_missing")).setEphemeral(true)
                        .queue()
                    return
                }


                val category = event.guild!!.getCategoryById(id)

                if (category == null) {
                    event.reply(getLocalizedString("buttonlistener.configuration_missing")).setEphemeral(true)
                        .queue()
                    return
                }

                val roleIds = event.member!!.roles.map { it.id }
                val permitted =
                    config.moderatorRoles.any { roleIds.contains(it) } || event.member!!.hasPermission(Permission.ADMINISTRATOR)


                if (config.maxTicketsPerUser != null && !permitted) {
                    if (ticketHolder.tickets.filter { it.creator == event.member!!.id }.size >= config.maxTicketsPerUser!!) {
                        event.reply(
                            getLocalizedString(
                                "buttonlistener.max_ticket_limit_error",
                                config.maxTicketsPerUser!!
                            )
                        ).setEphemeral(true).queue()
                        return
                    }
                }

                if (config.ticketKinds.isNotEmpty()) {
                    val options = config.ticketKinds.map { SelectOption(it.name, it.name) }
                    val selection = StringSelectMenu(
                        "BUTTON_CREATE_TICKET", getLocalizedString("buttonlistener.choose_kind.placeholder"),
                        options = options
                    )
                    event.reply(getLocalizedString("buttonlistener.choose_kind")).addActionRow(selection).setEphemeral(true).queue()
                    return
                }

                val ticket =
                    Ticket(0, config.defaultTicketName, "", event.member!!.id, mutableListOf(event.member!!.id))

                ticket.createChannel(category, config, ticketHolder)

                ticketHolder.addTicket(ticket)
                ticketHolder.save()

                event.deferEdit().queue()
            }

            "BUTTON_CLOSE_TICKET" -> {
                val tickets = GuildTicket.get(event.guild!!.id).tickets
                tickets.find { it.channelId == event.channel.id }?.close()
                event.deferEdit().queue()
            }
        }
    }

    override fun onStringSelectInteraction(event: StringSelectInteractionEvent) {
        val choice = event.interaction.selectedOptions[0].label
        when (event.component.id) {
            "BUTTON_CREATE_TICKET" -> {

                val config = ServerConfig.get(event.guild!!.id)
                val ticketHolder = GuildTicket.get(event.guild!!.id)
                val id = config.categoryId

                val category = event.guild!!.getCategoryById(id!!)


                val ticket =
                    Ticket(0, config.defaultTicketName, "", event.member!!.id, mutableListOf(event.member!!.id))

                ticket.createChannel(category!!, config, ticketHolder, config.ticketKinds.find { it.name == choice })

                ticketHolder.addTicket(ticket)
                ticketHolder.save()

                event.deferEdit().queue()

            }
        }
    }
}